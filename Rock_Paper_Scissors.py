"""Import required libraries"""
from random import randint

"""Provide list of values to play"""
list_of_values = ('Rock','Paper','Scissors')

"""Set up flag with default value to control the game"""
flag = 'Y'

"""Ask human to choose a value to play"""


while flag == 'Y':
    human = ''
    computer = list_of_values[randint(0,2)] # Assigning random value to computerRock
    while human not in list_of_values:
        human = input("Enter one of the following as your choice - Rock, Paper, Scissors\n")


    print('Computer picked: '+computer+'\n')
    if human == computer:
        print("It's a tie !")
    elif human == 'Rock' and computer == 'Paper':
        print('Computer wins ! Paper covers rock !!')
    elif human == 'Rock' and computer == 'Scissors':
        print('You win ! Rock crushes Scissors')
    elif human == 'Paper' and computer == 'Rock':
        print('You win ! Paper covers Rock')
    elif human == 'Paper' and computer == 'Scissors':
        print('Computer wins ! Scissors cut paper')
    elif human == 'Scissors' and computer =='Rock':
        print('Computer wins ! Rock crushes Scissors')
    elif human == 'Scissors' and computer =='Paper':
        print('You win ! Scissors cut Paper')


    flag = input('Enter Y if you want to continue the game. Enter any other key if you want to quit the game\n')
    

print('Thank you for playing !')

