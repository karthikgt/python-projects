#Import turtle
import turtle

window = turtle.Screen()
art = turtle.Turtle()

#Draw stalk in green pen
art.left(90)
art.color("green","black")
art.forward(100)
art.right(90)

#Draw petals
for i in range(1,24,1):
    art.color("black","blue")
    art.begin_fill()
    art.left(15)
    art.forward(50)
    art.left(157)
    art.forward(50)
    art.end_fill()

#Draw circle in black and fill it with orange
art.color("black","orange")
art.begin_fill()
art.circle(10)
art.end_fill()

art.hideturtle()

window.exitonclick()
