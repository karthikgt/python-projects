"""Tamil Practice Game"""
from random import shuffle

print("Welcome to Tamil Game. You will be quizzed to provide meaning for simple Tamil words \n")
print(" ")

tamil_words_with_meaning_simple = {
    "amma":"mom", "appa":"dad","oodu":"run", "saapidu":"eat"
}

counter = 'y'

while counter == 'y':
    #game_mode = input("Enter your choice for game mode: simple, medium, complex: ")
    #print("  ")
    #print("You selected "+game_mode.lower()+" mode\n")


    #if game_mode.lower() == "simple":
        question_list = list(tamil_words_with_meaning_simple.keys())
        shuffle(question_list)

        user_input = input("Provide meaning in English for "+ question_list[0]+": ")
        user_input.lower()

        if user_input == tamil_words_with_meaning_simple[question_list[0]]:
            print("Congratulations !! You won. The meaning for "+question_list[0]+ " is "+user_input)
            print("  ")
        else:
            print("Wrong answer !! The meaning for "+question_list[0]+ " is not "+user_input)
            print(" ")

        counter = input("Enter y if you want to continue to play. Else enter any other key :")








